# Truncate EDFs based on timing information #

Given a timing.csv file, with filename, lights off and lights on, truncate the EDF file and save it as file_name_truncated.edf

The header info should remain the same. Keep in mind the lights on time may refer to the next day. If the lights on is > duration of the EDF file, just read it till the end of the EDF. 

You will have to look at the pyedflib documentations: https://pyedflib.readthedocs.io/en/latest/ref/index.html

Some sample commands:
```python
edf_file = pyedflib.EdfReader(path)
labels = edf_file.getSignalLabels()
samples = edf_file.getNSamples()
start = edf_file.getStartdatetime()
```