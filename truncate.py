#!/usr/bin/python
# Script to Trucate EDF files based on lights on and lights off time
# Place all the EDF files in a folder along with a timing.csv file.
# Format for the timing file is:
# filename, lights off (HH:MM:SS),  lights on (HH:MM:SS)
# filename1.edf,    2:40:00,    5:00:00
# filename2.edf,    1:13:00,    6:20:00
# 
# python truncate.py -p /path/to/directory/with/files&timing
#
# This script is useful when you want the scoring to be perfectly aligned with 
# the lights off and lights on time.
#
# Written by Zhao Siting
#
# All rights reserved (c) 2021, Neurobit Technologies Pte Ltd
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os
import pyedflib
import pandas as pd
import datetime as dt
import argparse

def truncate(path):
    timing = pd.read_csv(os.path.join(path,'timing_test.csv'))
    timing['begin'] = timing['lights off (HH:MM:SS)'].apply(lambda x: pd.to_datetime(x))
    timing['end'] = timing['lights on (HH:MM:SS)'].apply(lambda x: pd.to_datetime(x))
    
    if not os.path.exists(os.path.join(path,'split-file/')):
        os.mkdir(os.path.join(path,'split-file/'))

    for _, row in timing.iterrows():
        filepath = row['filename']
        begin = row['begin']
        end = row['end']
        filename, ext = os.path.splitext(filepath)
        filename += '_truncated'
        saveName = filename + ext
        savePath = os.path.join(path,'split-file', saveName)
        filepath = os.path.join(path, filepath)
        bitmap = []

        with pyedflib.EdfReader(filepath) as edf_file:
            # reading all necessary info from edf file
            labels = edf_file.getSignalLabels()
            signalHeader = edf_file.getSignalHeaders()
            header = edf_file.getHeader()
            start = edf_file.getStartdatetime()
            sampfq = edf_file.getSampleFrequencies()
            keptSignal = []

            # adjusting timings
            begin = begin.replace(year=start.year, month=start.month, day=start.day)
            end = end.replace(year=start.year, month=start.month, day=start.day)

            if begin < start:
                begin += dt.timedelta(days=1)
                end += dt.timedelta(days=1)
            
            if end < begin:
                end += dt.timedelta(days=1)

            # truncating data
            for i, _ in enumerate(labels):
                # if sampling frequency == 0, skip the signal (error)
                if sampfq[i] == 0:
                    bitmap.append(0)
                    continue
                
                bitmap.append(1)
                signal = edf_file.readSignal(i)
                
                startIndex = int((begin-start).total_seconds() * sampfq[i])

                # if end timing is missing, read till EOF
                try:
                    endIndex = int((end-start).total_seconds() * sampfq[i])
                    # ends after signal ends, use last value
                    if endIndex > len(signal):
                        endIndex = len(signal)
                except:
                    endIndex = len(signal)
                
                # not including the sample at the end index 
                keptSignal.append(signal[startIndex:endIndex])

            newSignalHeader = [s for s, i in zip(signalHeader, bitmap) if i==1]
            header['startdate'] = begin
            pyedflib.highlevel.write_edf(savePath, keptSignal, newSignalHeader, header, file_type=0)
        
        # print CLI feedback to user
        print("Saved {}".format(saveName))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Truncates .edf file by given lights on and lights off (optional) timing')
    parser.add_argument('-p', '--file_path', dest='file_path', type=str, help='path to data and timing file (timing.csv)')
    args = parser.parse_args()
    path = args.file_path
    
    if not path:
        path = input("Enter the path where data and the timing file (timing.csv) are kept: ")
    
    if os.path.isdir(path):
        truncate(path)
    else:
        print('path is not a valid directory. Cannot continue.')